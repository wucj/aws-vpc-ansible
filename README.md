# Ansible

## Development Environment Requirements

Install ansible
  
```console
$ pip install ansible
```
Install awscli

```console
$ pip install awscli
```
Install boto
  
```console
$ pip install boto
```
Install boto3

```console
$ pip install boto3
```
Configure AWS related settings, please get the access key and secret key from aws console, then run

```console
$ aws configure
```
Create and get key pair in the EC2 Settings page

## Setup a VPC environment

Before run the playbook, please edit the settings under prod/staging directory according to your requirements
prod/staging directory is stored the arguments what infrastructure creating need, for example, vpc name, ip range of subnet
Here is the files in the prod directory
```
prod/
├── ec2.ini
├── ec2.py
├── group_vars
│   └── all
│       ├── ec2.yml
│       ├── iam.yml
│       ├── region.yml
│       ├── route.yml
│       ├── securitygroups.yml
│       ├── tags.yml
│       └── vpc.yml
└── host_vars
    └── jenkins.yml
```
Please specify the application name and env (prod or staging )of this infra in tags.yml, domain name in route.yml, region in region.yml, what is the iam role you would like to assign to ec2 in this VPC in iam.yml

### Create prod env.
```
$ ansible-playbook -i prod playbook/provision-infa.yml -vvvv -e ssh_key_name={YOUR KEY PAIR NAME} -e ec2_instance_profile_name={IAM ROLE NAME}
```
### Destroy prod env.
```
$ ansible-playbook -i prod playbook/decomission-infa.yml -vvvv
```

If you would like to have many applications in different VPC, you could copy prod directory to another directory and specify by using -i when you create another infra.
